package com.example.sandbox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.sandbox.fragments.MainFragment;
import com.example.sandbox.fragments.SecondFragment;
import com.example.sandbox.fragments.ThirdFragment;

import java.util.ArrayList;
import java.util.List;

import nl.joery.animatedbottombar.AnimatedBottomBar;

public class MainActivity extends AppCompatActivity {

    //variabel
    AnimatedBottomBar bottomBar;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup translucent status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        // inisialisasi variabel
        bottomBar = findViewById(R.id.bottom_bar);
        viewPager = findViewById(R.id.view_pager);
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<Integer> colorList = new ArrayList<>();

        //ini untuk textview yang ada di fragment
        // hapus aja biar bisa diganti sama konten asli fragmentnya
        arrayList.add("Tab 1");
        arrayList.add("Tab 2");
        arrayList.add("Tab 3");

        // list bckground color tiap fragment
        colorList.add(Color.RED);
        colorList.add(Color.GREEN);
        colorList.add(Color.BLUE);

        //setup adapter untuk viewpagernya
        prepareViewPager(viewPager, arrayList, colorList);

        //setup with viewpager
        bottomBar.setupWithViewPager(viewPager);
    }

    private void prepareViewPager(ViewPager viewPager, ArrayList<String> arrayList, ArrayList<Integer> colorList) {
        MainAdapter adapter = new MainAdapter(getSupportFragmentManager());


        MainFragment fragment = new MainFragment();
        SecondFragment secondFragment = new SecondFragment();
        ThirdFragment thirdFragment = new ThirdFragment();

        // main fragment
        Bundle bundle = new Bundle();
        bundle.putString("title", arrayList.get(0));
        fragment.setArguments(bundle);
        adapter.addFragment(fragment, arrayList.get(0));

        // second fragment
        bundle = new Bundle();
        bundle.putString("title", arrayList.get(1));
        secondFragment.setArguments(bundle);
        adapter.addFragment(secondFragment, arrayList.get(1));

        // third fragment
        bundle = new Bundle();
        bundle.putString("title", arrayList.get(2));
        thirdFragment.setArguments(bundle);
        adapter.addFragment(thirdFragment, arrayList.get(2));

        //kasih ke main adapter
        viewPager.setAdapter(adapter);
    }


    //inner class
    private class MainAdapter extends FragmentPagerAdapter {

        ArrayList<String> arrayList = new ArrayList<>();
        List<Fragment> fragmentList = new ArrayList<>();

        public void addFragment(Fragment fragment, String title) {
            arrayList.add(title);

            fragmentList.add(fragment);
        }

        public MainAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return arrayList.get(position);
        }
    }
}