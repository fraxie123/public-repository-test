package com.example.sandbox.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.example.sandbox.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private String[] sImportanceLevel = new String[] {"Penting", "Biasa", "Tidak Penting"};
    private String doSomethingString, importanceLevelString, importanceLevelString2;
    private TextInputEditText doSomething, remindMe;
    private AutoCompleteTextView importanceLevel;
    private MaterialButton submit;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.bottom_sheet, container, false);

        doSomething = view.findViewById(R.id.insertTodo);
        importanceLevel = view.findViewById(R.id.insertImportanceLevel);
        remindMe = view.findViewById(R.id.insertDate);
        submit = view.findViewById(R.id.btnAddActivity);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo : cari cara buat nyimpen date nya sesuai dengan date yang ada di database
                //todo : cari cara buat create notification sesuai dengan date yang udah disimpen
            }
        });

        remindMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //insialisasi datepicker
                MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();

                //kasih judul datepickernya
                builder.setTitleText("Remind me");

                //build datepickernya
                MaterialDatePicker<Long> picker = builder.build();

                //tampilin datepickernya
                picker.show(getFragmentManager(), picker.toString());

                picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                    @Override
                    public void onPositiveButtonClick(Long selection) {
//                        Toast.makeText(getContext(), picker.getHeaderText(), Toast.LENGTH_SHORT).show();
                        remindMe.setText(picker.getHeaderText());
                    }
                });
            }
        });

        //gk pake Objects.requireNonNull(this) karena ini konteksnya di fragment, bukan activity
        ArrayAdapter<String> adapterImportanceLevel = new ArrayAdapter<String>(getContext(), R.layout.list_item, R.id.item_list, sImportanceLevel);
        importanceLevel.setAdapter(adapterImportanceLevel);
        importanceLevel.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                importanceLevelString = sImportanceLevel[i];
            }
        });
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getContext(), getTheme());
    }
    public static class BottomSheetDialog extends com.google.android.material.bottomsheet.BottomSheetDialog {

        public BottomSheetDialog(@NonNull Context context) {
            super(context);
        }

        protected BottomSheetDialog(@NonNull Context context, final boolean cancelable,
                                    DialogInterface.OnCancelListener cancelListener) {
            super(context, cancelable, cancelListener);
        }

        public BottomSheetDialog(@NonNull Context context, @StyleRes final int theme) {
            super(context, theme);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            //customize width
            getWindow().setLayout(1000 /*our width*/, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }
}